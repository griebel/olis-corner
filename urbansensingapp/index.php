<!DOCTYPE html>
<html>
<head>
  <title>Urban Sensing App - Corner</title>

  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_common.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_urbansensingapp.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->

      <div class="content">
        <h1>Urban Sensing App</h1>
        <p>
          Eine App f&uuml;r Android-Ger&auml;te zur Erfassung von Wetter und
          Umweltverh&auml;ltnissen.
        </p>
        <h2>Geplante &Auml;nderungen</h2>
        <ul>
          <li>
            GUI soll optisch ansprechender gemacht werden
          </li>
          <li>
            Anpasssen der Ansprechzeit der Sensoren
          </li>
          <li>
            Aktivieren/Deaktivieren der App mittels Lagesensoren
          </li>
          <li>
            Aktuelle Lautst&auml;rke messen und aufzeichen
          </li>
          <li>
            Fehlermeldungen der Datenbank sollen erweitert angezeigt werden
          </li>
        </ul>
        <h2>Bekannte Bugs</h2>
        <ul>
          <li>
            Bei diversen Eingaben ohne Internetverbindung st&uuml;rzt die Anwendung ab
          </li>
          <li>
            Exportfunktion l&auml;sst Programm abst&uuml;rzen
          </li>
        </ul>
        <p>
          Weitere Bugs bitte an
          <a href='mailto:mail@olis-corner.de'>mail@olis-corner.de</a> senden.
        </p>
        <h2>Downloads</h2>
        <p>
          <a href="UrbanSensingApp.apk">App</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
</body>
</html>
