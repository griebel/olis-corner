<!DOCTYPE html>
<html>
<head>
	<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_tankmanager.inc.php'); ?>
</head>
<body>
	<div class="frame">
		<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
		<div class="body">
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_tankmanager_app.inc.php'); ?>
			<!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
			<!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
			<!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
			<!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
			<!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
			<div class="content">
				<h1>Tank Manager</h1>
				<p>
					Mit der App k&ouml;nnen sie ihre Tankbelege mobil abspeichern.
				</p>
				<p>
					Hierzu stellt ihnen die App ein Eingabeformular zur Verf&uuml;gung. Auch
					bereits abgespeicherte Belege k&ouml;nnen bearbeitetet werden. Eine anschlie&szlig;ende
					Analyse des Kraftstoffverbrauchs des Fahrzeugs ist auf der Website m&ouml;glich.
				</p>

				<h2>Download</h2>
				<p>
					Zurzeit befindet sich das System noch im Entwicklungsstatus. Daher ist die App noch
					nicht auf dem Market zu finden. Wer sie dennoch nutzen m&ouml;chte, kann sie hier
					downloaden und mit unten angegebener Erkl&auml;rung installieren.
				</p>
				<p>
					<a href="app/TankManager.apk">Download</a><br />
					Stand 20.07.2014
				</p>
				<h2>Installation</h2>
				<p>
					<ol>
						<li>
							Bevor Sie unter Android APKs aus anderen Quellen als dem Google Play Store
							installieren k&ouml;nnen, m&uuml;ssen Sie in den Systemeinstellungen die Einstellung
							"Unbekannte Quellen" aktivieren.
							<ul>
								<li>
									Gehen Sie in die Einstellungen
								</li>
								<li>
									W&auml;hlen Sie den Men&uuml;punkt "Sicherheit"
								</li>
								<li>
									Unter "Ger&auml;teverwaltung" finden Sie den Eintrag "Unbekannte Herkunft".<br />
									Setzen Sie das H&auml;kchen neben diesem Feld.
								</li>
								<li>
									Best&auml;tigen Sie die aufgehende Warnmeldung mit "OK".
								</li>
							</ul>
						</li>
						<li>
							Surfen Sie mit einem beliebigen Browser die Seite an, auf der die APK-Datei
							angeboten wird und starten Sie den Download.
						</li>
						<li>
							Den Fortschritt des Downloads k&ouml;nnen Sie unter den Benachrichtigungen verfolgen.
						</li>
						<li>
							Nach Abschluss des Downloads tippen Sie den entsprechenden Eintrag einmal an.
						</li>
						<li>
							Es &ouml;ffnet sich die Android-typische Installationsroutine. Mit einem Druck auf
							"Installieren" r&auml;umen Sie der App die angegebenen Rechte ein und starten die
							Installation.
						</li>
						<li>
							Die Installation sollte im Regelfall nur einige Sekunden und Anspruch nehmen und
							reibungslos verlaufen. Hat alles geklappt, k&ouml;nnen Sie die eben installierte
							App direkt &ouml;ffnen.
						</li>
					</ol>
				</p>
			</div>
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
		</div>
		<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
	</div>
</body>
</html>
