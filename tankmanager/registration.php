<!DOCTYPE html>
<html>
<head>
	<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_tankmanager.inc.php'); ?>
</head>
<body>
	<div class="frame">
		<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
		<div class="body">
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_tankmanager_registration.inc.php'); ?>
			<!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
			<!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
			<!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
			<!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
			<!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
			<div class="content">
				<h1>Tank Manager</h1>
				<p>
					Bitte wandeln sie Umlaute um.<br />
					Z.B. &auml; -> ae, &ouml; -> oe, &uuml; -> ue
				</p>

				<p>
					<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						<table>
							<tr>
								<td>Nummernschild:</td>
								<td><input type="text" name="nummernschild" maxlength="22" /></td>
							</tr>
							<tr>
								<td>Passwort:</td>
								<td><input type="password" name="passwort" maxlength="22" /></td>
							</tr>
							<tr>
								<td>Passwort wiederholen:</td>
								<td><input type="password" name="passwort2" maxlength="22" /></td>
							</tr>
							<tr>
								<td>Kilometerstand:</td>
								<td><input type="text" name="kilometerstand" maxlength="6" /></td>
							</tr>
							<tr>
								<td></td>
								<td><input type="submit" name="submit" value="Registrieren" /></td>
							</tr>
						</table>
					</form>
				</p>

				<?php
				if(isset($_POST['submit'])) {
					// Implementieren des Datenbankaufrufes
					require 'includes/registration.func.php';

					$nummernschild = $_POST["nummernschild"];
					$passwort = $_POST ["passwort"];
					$passwort2 = $_POST ["passwort2"];
					$kilometerstand = $_POST ["kilometerstand"];

					$registration = registration ( $nummernschild, $passwort, $passwort2, $kilometerstand );

					// War Registrierung erfolgreich
					if ($registration == "success") {
						?>
						<p>
							Registration erfolgreich!<br />
							Ihr Loginname lautet: "<?php //echo $nummernschild;?>"<br />
							<a href="index.php">zur&uuml;ck zum Login</a>
						</p>
						<?php
					} else if ($registration == "wrongPassword") {
						?>
						<p>
							Deine Passw&ouml;rter stimmen nicht &uuml;berein. Bitte wiederhole deine Eingabe...<br />
							<a href="registration.php">zur&uuml;ck</a>
						</p>
						<?php
					} else if ($registration == "empty") {
						?>
						<p>
							Es wurde nichts eingegeben...<br />
							<a href="registration.php">zur&uuml;ck</a>
						</p>
						<?php
					} else if ($registration == "exists") {
						?>
						<p>
							Das Fahrzeug ist bereits registriert. Bitte wiederhole deine Eingabe...<br />
							<a href="registration.php">zur&uuml;ck</a>
						</p>
						<?php
					} else {
						?>
						<p>
							Es ist ein Fehler aufgetreten. Bitte probieren Sie es erneut.<br />
							<a href="registration.php">zur&uuml;ck</a>
						</p>
						<?php
					}
				}
				?>
			</div>
			<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
		</div>
		<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
	</div>
</body>
</html>
