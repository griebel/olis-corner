<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_tankmanager.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_tankmanager_home.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Tank Manager</h1>
        <p>
          Mit den Tankmanager k&ouml;nnen sie Ihre Tankquittungen digitalisieren
          und haben immer einen &Uuml;berblick &uuml;ber Kosten und Verbrauch.
        </p>

        <h2>Wartungsarbeiten</h2>
        <p>
          Alle Daten werden bald wieder zur Verf&uuml;gung stehen.
        </p>

        <p>
          Bitte logge dich ein:<br />
          <form method="post" action="index.php?page=tankmanager2">
            <table  class="tableborderless">
              <tr>
                <td>User:</td>
                <td><input type="text" name="nummernschild" /></td>
              </tr>
              <tr>
                <td>Passwort:</td>
                <td><input type="password" name="passwort" /></td>
              </tr>
              <tr>
                <td></td>
                <td><input type="submit" value="Einloggen" /></td>
              </tr>
            </table>
          </form>
        </p>

        <p>
          <a href="registration.php">
            Noch nicht dabei? Jetzt registrieren...
          </a>
        </p>

        <!-- <h2>Mal wieder tanken gewesen?</h2>
        <p>
        Geben Sie ihre neue Tankquittung hier ein!
      </p>
      <p>
      Bitte unbedingt die Formatvorlage beachten!
      <form method="post" action="index.php?page=tankmanager_index2">
      <table class="tableborderless">
      <tr>
      <td></td>
      <td></td>
      <td>Format:</td>
    </tr>
    <tr>
    <td>Datum:</td>
    <td><input type="text" name="datum" placeholder="20.05.2016"/></td>
  </tr>
  <tr>
  <td>Kilometerstand:</td>
  <td><input type="text" name="kilometerstand" placeholder="115216"/></td>
</tr>
<tr>
<td>Literpreis:</td>
<td><input type="text" name="literpreis" placeholder="1.49"/></td>
</tr>
<tr>
<td>Getankte Menge:</td>
<td><input type="text" name="menge" placeholder="48.5"/></td>
</tr>
<tr>
<td></td>
<td><input type="submit" value="Speichern" /></td>
</tr>
</table>
</form>
</p> -->
</div>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
</div>
<?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
</div>
</body>
</html>
