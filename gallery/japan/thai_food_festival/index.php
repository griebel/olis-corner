<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Thai Food Festival</h1>
        <p>
          Ein Festival im Yoyogi Park rund um das Thail&auml;ndische Essen.
          Ausgezeichnet!<br>
          Und im Park nebenan war auch noch ein Zombiewalk.<br>
          Anschlie&szlig;end ging es zum Karaoke singen.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('thai_food_festival');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2715', '', '');
          $gallery->addPopupImage('IMG_2717', '', '');
          $gallery->addPopupImage('IMG_2741', '', '');
          $gallery->addPopupImage('IMG_2742', '', '');
          $gallery->addPopupImage('IMG_2743', '', '');
          $gallery->addPopupImage('IMG_2744', '', '');
          $gallery->addPopupImage('IMG_2753', '', '');
          $gallery->addPopupImage('IMG_2755', '', '');
          $gallery->addPopupImage('IMG_2756', '', '');
          $gallery->addPopupImage('IMG_2757', '', '');
          $gallery->addPopupImage('IMG_2758', '', '');
          $gallery->addPopupImage('IMG_2759', '', '');
          $gallery->addPopupImage('IMG_2760', '', '');
          $gallery->addPopupImage('IMG_2765', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
