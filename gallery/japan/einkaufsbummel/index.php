<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Einkaufsbummel</h1>
        <p>
          Ich bin mal an meinem freien Tag zeitig aufgestanden und in die Stadt,
          auf der Suche nach einem Musikplayer. Die Rushhour nat&uuml;rlich
          mitgenommen, Fr&uuml;hst&uuml;ck im Subway und zuf&auml;llig eine super lustige
          Schweizerin getroffen. Mit ihr bin ich dann auch weiter gezogen, wobei
          ich einige coole Dinge gefunden und gekauft habe. Der erw&uuml;nschte
          Player ist leider nicht darunter gewesen.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('einkaufsbummel');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3156', '', '');
          $gallery->addPopupImage('IMG_3160', '', '');
          $gallery->addPopupImage('IMG_3162', '', '');
          $gallery->addPopupImage('IMG_3165', '', '');
          $gallery->addPopupImage('IMG_3167', '', '');
          $gallery->addPopupImage('IMG_3168', '', '');
          $gallery->addPopupImage('IMG_3169', '', '');
          $gallery->addPopupImage('IMG_3170', '', '');
          $gallery->addPopupImage('IMG_3171', '', '');
          $gallery->addPopupImage('IMG_3173', '', '');
          $gallery->addPopupImage('IMG_3174', '', '');
          $gallery->addPopupImage('IMG_3177', '', '');
          $gallery->addPopupImage('IMG_3178', '', '');
          $gallery->addPopupImage('IMG_3182', '', '');
          $gallery->addPopupImage('IMG_3186', '', '');
          $gallery->addPopupImage('IMG_3188', '', '');
          $gallery->addPopupImage('IMG_3190', '', '');
          $gallery->addPopupImage('IMG_3192', '', '');
          $gallery->addPopupImage('IMG_3194', '', '');
          $gallery->addPopupImage('IMG_3198', '', '');
          $gallery->addPopupImage('IMG_3200', '', '');
          $gallery->addPopupImage('IMG_3201', '', '');
          $gallery->addPopupImage('IMG_3205', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
