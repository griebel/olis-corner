<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Uni Tour</h1>
        <p>
          Unsere japanischen Komillitonen zeigen uns die Universit&auml;t.
          Anschlie&szlig;end geht es Essen und in ein Onsen (hei&szlig;e Quelle) mit Peter.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('uni_tour');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_1978', '', '');
          $gallery->addPopupImage('IMG_1979', '', '');
          $gallery->addPopupImage('IMG_1981', '', '');
          $gallery->addPopupImage('IMG_1984', '', '');
          $gallery->addPopupImage('IMG_1987', '', '');
          $gallery->addPopupImage('IMG_1989', '', '');
          $gallery->addPopupImage('IMG_2001', '', '');
          $gallery->addPopupImage('IMG_2002', '', '');
          $gallery->addPopupImage('IMG_2006', '', '');
          $gallery->addPopupImage('IMG_2009', '', '');
          $gallery->addPopupImage('IMG_2014', '', '');
          $gallery->addPopupImage('IMG_2020', '', '');
          $gallery->addPopupImage('IMG_2027', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
