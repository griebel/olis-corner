<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Meine Unterkunft</h1>
        <p>
          Also es sind definitiv weniger als 10m&sup2; hier. Aber wenigstens
          habe ich einen Balkon, der etwa halb so gro&szlig; ist wie das Zimmer.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('meine_unterkunft');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_1961', '', '');
          $gallery->addPopupImage('IMG_1962', '', '');
          $gallery->addPopupImage('IMG_1967', '', '');
          $gallery->addPopupImage('IMG_1968', '', '');
          $gallery->addPopupImage('IMG_1969', '', '');
          $gallery->addPopupImage('IMG_1970', '', '');
          $gallery->addPopupImage('IMG_2037', '', '');
          $gallery->addPopupImage('IMG_2166', '', '');
          $gallery->addPopupImage('IMG_2211', '', '');
          $gallery->addPopupImage('IMG_2846', '', '');
          $gallery->addPopupImage('IMG_2849', '', '');
          $gallery->addPopupImage('IMG_2851', '', '');
          $gallery->addPopupImage('IMG_2853', '', '');
          $gallery->addPopupImage('IMG_2854', '', '');
          $gallery->addPopupImage('IMG_2856', '', '');
          $gallery->addPopupImage('IMG_2857', '', '');
          $gallery->addPopupImage('20150404_185057', '', '');
          $gallery->addPopupImage('20150515_203951', '', '');
          $gallery->addPopupImage('20150515_223050', '', '');
          $gallery->addPopupImage('20150524_002149', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
