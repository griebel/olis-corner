<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Jonas 22ter</h1>
        <p>
          Die Chance, ein Maid-Cafe von innen zu begutachten. Ich kann nur
          sagen, dass ich da nicht noch einmal hinein muss. Extrem gru&szlig;elig!
          Ich kam mir etwas fehl am Platz vor. Die Programmpalette geht vom
          singen eines liedes auf der B&uuml;hne bis zum Rollen von Sushi direkt
          auf dem Tisch.<br>
          Anschlie&szlig;end ging es in ein Izakaya, all you can drink f&uuml;r 2 Stunden,
          nur rund 15€. Leider war das Essen nicht inklusive.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('jonas_22ter');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2858', '', '');
          $gallery->addPopupImage('IMG_2860', '', '');
          $gallery->addPopupImage('IMG_2862', '', '');
          $gallery->addPopupImage('IMG_2863', '', '');
          $gallery->addPopupImage('IMG_2866', '', '');
          $gallery->addPopupImage('IMG_2869', '', '');
          $gallery->addPopupImage('IMG_2874', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
