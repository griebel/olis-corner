<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Der erste Tag</h1>
        <p>
          Mein erster Tag in Japan. Alles ist hier anders. Meine neuen
          Kommilitonen sind allerdings genau wie ich sehr aufgeregt, was Japan
          wohl alles f&uuml;r uns zu bieten hat.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('der_erste_tag');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_1960', '', '');
          $gallery->addPopupImage('IMG_1963', '', '');
          $gallery->addPopupImage('IMG_1964', '', '');
          $gallery->addPopupImage('IMG_1971', '', '');
          $gallery->addPopupImage('20150403_213241', '', '');
          $gallery->addPopupImage('20150403_214059', '', '');
          $gallery->addPopupImage('20150403_214147', '', '');
          $gallery->addPopupImage('20150403_214543', '', '');
          $gallery->addPopupImage('20150403_214700', '', '');
          $gallery->addPopupImage('20150403_222744', '', '');
          $gallery->addPopupImage('20150403_222836', '', '');
          $gallery->addPopupImage('20150403_232314', '', '');
          $gallery->addPopupImage('20150403_232542', '', '');
          $gallery->addPopupImage('20150403_232816', '', '');
          $gallery->addPopupImage('20150403_233644', '', '');
          $gallery->addPopupImage('20150403_234335', '', '');
          $gallery->addPopupImage('20150403_234540', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
