<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>FUJI-Q Highland Park</h1>
        <p>
          Ein gigantischer Erlebnispark! So die Theorie. Nach der Erwartung
          eines Europaparks haben wir einen Holidaypark gefunden. Allerdings
          sind die Achterbahnen um welten besser als die Deutschen!
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('fuji_q_highland_park');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3140', '', '');
          $gallery->addPopupImage('FUJIQ_62', '', '');
          $gallery->addPopupImage('FUJIQ_1661', '', '');
          $gallery->addPopupImage('FUJIQ_1712', '', '');
          $gallery->addPopupImage('FUJIQ_2299', '', '');
          $gallery->addPopupImage('FUJIQ_4423', '', '');
          $gallery->addPopupImage('FUJIQ_4624', '', '');
          $gallery->addPopupImage('FUJIQ_4920', '', '');
          $gallery->addPopupImage('FUJIQ_6315', '', '');
          $gallery->addPopupImage('FUJIQ_7041', '', '');
          $gallery->addPopupImage('FUJIQ_8303', '', '');
          $gallery->addPopupImage('FUJIQ_8772', '', '');
          $gallery->addPopupImage('FUJIQ_9671', '', '');
          $gallery->addPopupImage('IMG_3087', '', '');
          $gallery->addPopupImage('IMG_3108', '', '');
          $gallery->addPopupImage('IMG_3113', '', '');
          $gallery->addPopupImage('IMG_3117', '', '');
          $gallery->addPopupImage('IMG_3127', '', '');
          $gallery->addPopupImage('IMG_3129', '', '');
          $gallery->addPopupImage('IMG_3134', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
