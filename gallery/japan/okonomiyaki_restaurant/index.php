<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Okonomiyaki Restaurant</h1>
        <p>
          Okonomiyaki ist &auml;hnlich wie ein Pfannkuchen. Der Unterschied ist aber,
          dass hier Zutaten sehr variabel sind und in diesem All-You-Can-Eat
          Restaurant der Service hervorragend war.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('okonomiyaki_restaurant');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150525_193901', '', '');
          $gallery->addPopupImage('20150525_193936', '', '');
          $gallery->addPopupImage('20150525_194154', '', '');
          $gallery->addPopupImage('20150525_194316', '', '');
          $gallery->addPopupImage('20150525_194822', '', '');
          $gallery->addPopupImage('20150525_201847', '', '');
          $gallery->addPopupImage('20150525_203938', '', '');
          $gallery->addPopupImage('20150525_204804', '', '');
          $gallery->addPopupImage('20150525_204808', '', '');
          $gallery->addPopupImage('20150525_205346', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
