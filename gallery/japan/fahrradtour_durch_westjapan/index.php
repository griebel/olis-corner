<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Fahrradtour durch Westjapan</h1>
        <p>
          1. Mai - 10. Mai
          Einmal quer durch den Westen Japans.<br>
          Gestartet mit dem Shinkanzen (Zug) von Tokio nach Kyoto. Wir sind
          durch das Inselreich &uuml;ber die S&uuml;dinseln Shikoku und Kyûshû nach Izumo
          geradelt. Dabei haben wir allerlei kurioses gesehen, wunderbares
          erlebt und traumhaftes gegessen. Einen kleinen Einblick in dieses
          10-t&auml;gige Abenteuer gibt es hier.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('fahrradtour_durch_westjapan');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('bild_route', '', '');
          $gallery->addPopupImage('DSCN0016', '', '');
          $gallery->addPopupImage('IMG_2406', '', '');
          $gallery->addPopupImage('IMG_2411', '', '');
          $gallery->addPopupImage('IMG_2419', '', '');
          $gallery->addPopupImage('IMG_2421', '', '');
          $gallery->addPopupImage('DSCN0017', '', '');
          $gallery->addPopupImage('IMG_2425', '', '');
          $gallery->addPopupImage('IMG_20150502_063352', '', '');
          $gallery->addPopupImage('IMG_20150502_073208', '', '');
          $gallery->addPopupImage('IMG_2426', '', '');
          $gallery->addPopupImage('IMG_20150502_080346', '', '');
          $gallery->addPopupImage('IMG_2428', '', '');
          $gallery->addPopupImage('IMG_2434', '', '');
          $gallery->addPopupImage('IMG_2437', '', '');
          $gallery->addPopupImage('IMG_2441', '', '');
          $gallery->addPopupImage('IMG_2444', '', '');
          $gallery->addPopupImage('IMG_2446', '', '');
          $gallery->addPopupImage('IMG_2451', '', '');
          $gallery->addPopupImage('IMG_2454', '', '');
          $gallery->addPopupImage('IMG_2489', '', '');
          $gallery->addPopupImage('IMG_2472', '', '');
          $gallery->addPopupImage('IMG_2476', '', '');
          $gallery->addPopupImage('IMG_2477', '', '');
          $gallery->addPopupImage('IMG_20150502_113340', '', '');
          $gallery->addPopupImage('IMG_2481', '', '');
          $gallery->addPopupImage('DSCN0055', '', '');
          $gallery->addPopupImage('IMG_2492', '', '');
          $gallery->addPopupImage('IMG_20150502_214005', '', '');
          $gallery->addPopupImage('IMG_2495', '', '');
          $gallery->addPopupImage('DSCN0064', '', '');
          $gallery->addPopupImage('DSCN0059', '', '');
          $gallery->addPopupImage('IMG_2498', '', '');
          $gallery->addPopupImage('IMG_2503', '', '');
          $gallery->addPopupImage('IMG_20150503_130111', '', '');
          $gallery->addPopupImage('IMG_20150503_130944', '', '');
          $gallery->addPopupImage('IMG_20150503_131601', '', '');
          $gallery->addPopupImage('IMG_20150503_131606', '', '');
          $gallery->addPopupImage('IMG_20150503_133247', '', '');
          $gallery->addPopupImage('IMG_20150503_133417', '', '');
          $gallery->addPopupImage('IMG_20150503_133846', '', '');
          $gallery->addPopupImage('IMG_20150503_135126', '', '');
          $gallery->addPopupImage('DSCN0066', '', '');
          $gallery->addPopupImage('DSCN0068', '', '');
          $gallery->addPopupImage('DSCN0086', '', '');
          $gallery->addPopupImage('IMG_2512', '', '');
          $gallery->addPopupImage('IMG_2514', '', '');
          $gallery->addPopupImage('IMG_2515', '', '');
          $gallery->addPopupImage('DSCN0097', '', '');
          $gallery->addPopupImage('IMG_20150504_205110', '', '');
          $gallery->addPopupImage('DSCN0109', '', '');
          $gallery->addPopupImage('DSCN0116', '', '');
          $gallery->addPopupImage('DSCN0129', '', '');
          $gallery->addPopupImage('P1010651', '', '');
          $gallery->addPopupImage('P1010657', '', '');
          $gallery->addPopupImage('P1010660', '', '');
          $gallery->addPopupImage('P1010667', '', '');
          $gallery->addPopupImage('IMG_20150505_073542', '', '');
          $gallery->addPopupImage('P1010672', '', '');
          $gallery->addPopupImage('P1010686', '', '');
          $gallery->addPopupImage('P1010702', '', '');
          $gallery->addPopupImage('IMG_2524', '', '');
          $gallery->addPopupImage('DSCN0136', '', '');
          $gallery->addPopupImage('DSCN0146', '', '');
          $gallery->addPopupImage('IMG_20150505_132348', '', '');
          $gallery->addPopupImage('DSCN0181', '', '');
          $gallery->addPopupImage('IMG_2534', '', '');
          $gallery->addPopupImage('IMG_2546', '', '');
          $gallery->addPopupImage('IMG_2554', '', '');
          $gallery->addPopupImage('IMG_2561', '', '');
          $gallery->addPopupImage('IMG_2565', '', '');
          $gallery->addPopupImage('IMG_2569', '', '');
          $gallery->addPopupImage('DSCN0207', '', '');
          $gallery->addPopupImage('IMG_2570', '', '');
          $gallery->addPopupImage('IMG_2575', '', '');
          $gallery->addPopupImage('IMG_2583', '', '');
          $gallery->addPopupImage('IMG_2589', '', '');
          $gallery->addPopupImage('IMG_2598', '', '');
          $gallery->addPopupImage('IMG_2603', '', '');
          $gallery->addPopupImage('IMG_2604', '', '');
          $gallery->addPopupImage('DSCN0258', '', '');
          $gallery->addPopupImage('IMG_2606', '', '');
          $gallery->addPopupImage('IMG_2619', '', '');
          $gallery->addPopupImage('IMG_20150506_180425', '', '');
          $gallery->addPopupImage('DSCN0266', '', '');
          $gallery->addPopupImage('IMG_2620', '', '');
          $gallery->addPopupImage('IMG_2621', '', '');
          $gallery->addPopupImage('IMG_20150507_121448', '', '');
          $gallery->addPopupImage('IMG_2622', '', '');
          $gallery->addPopupImage('IMG_2623', '', '');
          $gallery->addPopupImage('DSCN0279', '', '');
          $gallery->addPopupImage('IMG_2625', '', '');
          $gallery->addPopupImage('IMG_2634', '', '');
          $gallery->addPopupImage('DSCN0335', '', '');
          $gallery->addPopupImage('IMG_20150507_151520', '', '');
          $gallery->addPopupImage('IMG_20150508_134209', '', '');
          $gallery->addPopupImage('IMG_2639', '', '');
          $gallery->addPopupImage('IMG_2649', '', '');
          $gallery->addPopupImage('IMG_2656', '', '');
          $gallery->addPopupImage('IMG_2660', '', '');
          $gallery->addPopupImage('IMG_2663', '', '');
          $gallery->addPopupImage('IMG_2669', '', '');
          $gallery->addPopupImage('IMG_2672', '', '');
          $gallery->addPopupImage('IMG_20150510_083300', '', '');
          $gallery->addPopupImage('IMG_2675', '', '');
          $gallery->addPopupImage('IMG_2677', '', '');
          $gallery->addPopupImage('IMG_2688', '', '');
          $gallery->addPopupImage('IMG_2691', '', '');
          $gallery->addPopupImage('IMG_2700', '', '');
          $gallery->addPopupImage('IMG_2706', '', '');
          $gallery->addPopupImage('IMG_2709', '', '');
          $gallery->addPopupImage('IMG_2713', '', '');
          $gallery->addPopupImage('DSCN0392', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
