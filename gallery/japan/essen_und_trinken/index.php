<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Essen &amp; Trinken</h1>
        <p>
          Hier mal einige Eindr&uuml;cke von dem, was es in Japan alles leckeres
          zu essen gibt!<br>
          Und das ist ein Haufen!!!
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('essen_und_trinken');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150404_134631', '', '');
          $gallery->addPopupImage('20150404_142051', '', '');
          $gallery->addPopupImage('20150404_142059', '', '');
          $gallery->addPopupImage('20150409_161803', '', '');
          $gallery->addPopupImage('20150409_175453', '', '');
          $gallery->addPopupImage('20150409_183833', '', '');
          $gallery->addPopupImage('20150409_194403', '', '');
          $gallery->addPopupImage('20150410_122820', '', '');
          $gallery->addPopupImage('20150410_123446', '', '');
          $gallery->addPopupImage('20150410_165207', '', '');
          $gallery->addPopupImage('20150412_215157', '', '');
          $gallery->addPopupImage('20150418_045912', '', '');
          $gallery->addPopupImage('20150418_204422', '', '');
          $gallery->addPopupImage('20150418_232915', '', '');
          $gallery->addPopupImage('20150421_172051', '', '');
          $gallery->addPopupImage('20150424_180722', '', '');
          $gallery->addPopupImage('20150428_103234', '', '');
          $gallery->addPopupImage('20150428_222139', '', '');
          $gallery->addPopupImage('20150429_204342', '', '');
          $gallery->addPopupImage('20150515_201811', '', '');
          $gallery->addPopupImage('20150520_205242', '', '');
          $gallery->addPopupImage('20150526_083946', '', '');
          $gallery->addPopupImage('20150526_211949', '', '');
          $gallery->addPopupImage('20150527_090848', '', '');
          $gallery->addPopupImage('20150529_123026', '', '');
          $gallery->addPopupImage('20150529_230805', '', '');
          $gallery->addPopupImage('20150531_142252', '', '');
          $gallery->addPopupImage('20150531_220126', '', '');
          $gallery->addPopupImage('20150603_101935', '', '');
          $gallery->addPopupImage('20150606_145201', '', '');
          $gallery->addPopupImage('IMG_1972', '', '');
          $gallery->addPopupImage('IMG_1973', '', '');
          $gallery->addPopupImage('IMG_1974', '', '');
          $gallery->addPopupImage('IMG_1993', '', '');
          $gallery->addPopupImage('IMG_1994', '', '');
          $gallery->addPopupImage('IMG_1998', '', '');
          $gallery->addPopupImage('IMG_1999', '', '');
          $gallery->addPopupImage('IMG_2000', '', '');
          $gallery->addPopupImage('IMG_2021', '', '');
          $gallery->addPopupImage('IMG_2022', '', '');
          $gallery->addPopupImage('IMG_2024', '', '');
          $gallery->addPopupImage('IMG_2033', '', '');
          $gallery->addPopupImage('IMG_2034', '', '');
          $gallery->addPopupImage('IMG_2069', '', '');
          $gallery->addPopupImage('IMG_2124', '', '');
          $gallery->addPopupImage('IMG_2125', '', '');
          $gallery->addPopupImage('IMG_2150', '', '');
          $gallery->addPopupImage('IMG_2153', '', '');
          $gallery->addPopupImage('IMG_2168', '', '');
          $gallery->addPopupImage('IMG_2171', '', '');
          $gallery->addPopupImage('IMG_2176', '', '');
          $gallery->addPopupImage('IMG_2177', '', '');
          $gallery->addPopupImage('IMG_2180', '', '');
          $gallery->addPopupImage('IMG_2185', '', '');
          $gallery->addPopupImage('IMG_2186', '', '');
          $gallery->addPopupImage('IMG_2187', '', '');
          $gallery->addPopupImage('IMG_2188', '', '');
          $gallery->addPopupImage('IMG_2199', '', '');
          $gallery->addPopupImage('IMG_2201', '', '');
          $gallery->addPopupImage('IMG_2208', '', '');
          $gallery->addPopupImage('IMG_2402', '', '');
          $gallery->addPopupImage('IMG_2403', '', '');
          $gallery->addPopupImage('IMG_2844', '', '');
          $gallery->addPopupImage('IMG_3073', '', '');
          $gallery->addPopupImage('IMG_3078', '', '');
          $gallery->addPopupImage('IMG_3082', '', '');
          $gallery->addPopupImage('IMG_3141', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
