<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Media Cafe Popeye</h1>
        <p>
          &Uuml;bernachten in einem Mangacafe. Dieses Cafe hatte in etwa 1 Milliarde
          Manga Hefte. Wahrscheinlich alle, die je geschrieben wurden. Und diese
          waren nat&uuml;rlich zur freien Verf&uuml;gung lesbar.<br>
          Eine sehr g&uuml;nstige Alternative. Vor allem gibt es Kaffee und
          Softdrinks all-you-can-drink. Mir war gnadenlos schlecht.<br>
          Da bin ich doch direkt Mitglied in dem Club geworden.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('media_cafe_popeye');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_5041', '', '');
          $gallery->addPopupImage('IMG_5046', '', '');
          $gallery->addPopupImage('IMG_5048', '', '');
          $gallery->addPopupImage('IMG_5050', '', '');
          $gallery->addPopupImage('IMG_5054', '', '');
          $gallery->addPopupImage('IMG_5055', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
