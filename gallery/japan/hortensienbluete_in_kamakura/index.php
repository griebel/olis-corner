<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Hortensienbl&uuml;te in Kamakura</h1>
        <p>
          Kamakura ist eine Stadt nahe Tokio, genau genommen noch im
          Ballungsraum. Dies merkt man an einem Tag am Wochenende um dies
          Jahreszeit. Gef&uuml;hlt alle Menschen aus Tokio kommen an diesen Ort um
          die Hortensienbl&uuml;te zu erleben und milliarden Bilder zu schie&szlig;en.
          Logisch, dass da auch mindestens ein Deutscher dabei sein muss.
          Allerdings hat Kamakura durchaus mehr zu bieten, wir sind &uuml;ber einen
          Wanderweg an den gro&szlig;en Buddha gekommen. Wobei der Wanderweg weniger
          weg und mehr matsch war. Gerade durch die Regenzeit und falsches
          Schuhwerk beeinflusst sehr m&uuml;&szlig;ig. Allerdings entsch&auml;digt der Anblick
          des gro&szlig;en Buddha und der Strand ein wenig. Sp&auml;testens aber das
          Thail&auml;ndische Essen wiegt wieder alles auf, traumhaft!
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('hortensienbluete_in_kamakura');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3206', '', '');
          $gallery->addPopupImage('IMG_3213', '', '');
          $gallery->addPopupImage('IMG_3228', '', '');
          $gallery->addPopupImage('IMG_3230', '', '');
          $gallery->addPopupImage('IMG_3234', '', '');
          $gallery->addPopupImage('IMG_3243', '', '');
          $gallery->addPopupImage('IMG_3246', '', '');
          $gallery->addPopupImage('IMG_3249', '', '');
          $gallery->addPopupImage('IMG_3255', '', '');
          $gallery->addPopupImage('IMG_3261', '', '');
          $gallery->addPopupImage('IMG_3267', '', '');
          $gallery->addPopupImage('IMG_3271', '', '');
          $gallery->addPopupImage('IMG_3277', '', '');
          $gallery->addPopupImage('IMG_3286', '', '');
          $gallery->addPopupImage('IMG_3288', '', '');
          $gallery->addPopupImage('IMG_3290', '', '');
          $gallery->addPopupImage('IMG_3296', '', '');
          $gallery->addPopupImage('IMG_3297', '', '');
          $gallery->addPopupImage('IMG_3298', '', '');
          $gallery->addPopupImage('IMG_3299', '', '');
          $gallery->addPopupImage('IMG_3300', '', '');
          $gallery->addPopupImage('IMG_3301', '', '');
          $gallery->addPopupImage('IMG_3303', '', '');
          $gallery->addPopupImage('IMG_3310', '', '');
          $gallery->addPopupImage('IMG_3315', '', '');
          $gallery->addPopupImage('IMG_3322', '', '');
          $gallery->addPopupImage('IMG_3324', '', '');
          $gallery->addPopupImage('IMG_3326', '', '');
          $gallery->addPopupImage('IMG_3327', '', '');
          $gallery->addPopupImage('IMG_3328', '', '');
          $gallery->addPopupImage('IMG_3333', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
