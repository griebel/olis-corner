<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Mount Fuji</h1>
        <p>
          Eine wunderbare Nachtwanderung auf den Mount Fuji, oder auch Fujisan.
          Mit 3700 was auch immer meter der h&ouml;chste Berg Japans. Eine absolut
          empfehlenswerte Wanderung!<br>
          Leider war das Wetter nicht gerade gut und der erwartete Sonnenaufgang
          ist etwas knapp ausgefallen. Genau genommen hat es durchgehend
          ziemlich heftig geregnet, es war extrem windig und sehr dichter Nebel.
          Der Sonnenaufgang war nicht zu sehen. Ich glaube ich muss den Berg
          wohl ein ander mal erneut bezwingen. Leider ist es nur erlaubt
          zwischen Juli und September hier zu wandern.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('mount_fuji');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3560', '', '');
          $gallery->addPopupImage('IMG_3561', '', '');
          $gallery->addPopupImage('IMG_3563', '', '');
          $gallery->addPopupImage('IMG_3564', '', '');
          $gallery->addPopupImage('IMG_3570', '', '');
          $gallery->addPopupImage('IMG_3571', '', '');
          $gallery->addPopupImage('IMG_3581', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
