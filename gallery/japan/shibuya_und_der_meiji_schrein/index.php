<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Shibuja und der Meiji-Schrein</h1>
        <p>
          Besuch des Tokio Metropolitan Government Buildings, einer planlosen
          Tour durch die Stadt mit der Entdeckung eines Schreins und dem Besuch
          des Meiji-Schreins.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('shibuya_und_der_meiji_schrein');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2218', '', '');
          $gallery->addPopupImage('IMG_2221', '', '');
          $gallery->addPopupImage('IMG_2222', '', '');
          $gallery->addPopupImage('IMG_2237', '', '');
          $gallery->addPopupImage('IMG_2238', '', '');
          $gallery->addPopupImage('IMG_2243', '', '');
          $gallery->addPopupImage('IMG_2249', '', '');
          $gallery->addPopupImage('IMG_2251', '', '');
          $gallery->addPopupImage('IMG_2256', '', '');
          $gallery->addPopupImage('IMG_2259', '', '');
          $gallery->addPopupImage('IMG_2261', '', '');
          $gallery->addPopupImage('IMG_2265', '', '');
          $gallery->addPopupImage('IMG_2275', '', '');
          $gallery->addPopupImage('IMG_2277', '', '');
          $gallery->addPopupImage('IMG_2279', '', '');
          $gallery->addPopupImage('IMG_2283', '', '');
          $gallery->addPopupImage('IMG_2288', '', '');
          $gallery->addPopupImage('IMG_2290', '', '');
          $gallery->addPopupImage('IMG_2292', '', '');
          $gallery->addPopupImage('IMG_2293', '', '');
          $gallery->addPopupImage('IMG_2295', '', '');
          $gallery->addPopupImage('IMG_2296', '', '');
          $gallery->addPopupImage('IMG_2300', '', '');
          $gallery->addPopupImage('IMG_2305', '', '');
          $gallery->addPopupImage('IMG_2306', '', '');
          $gallery->addPopupImage('IMG_2309', '', '');
          $gallery->addPopupImage('IMG_2312', '', '');
          $gallery->addPopupImage('IMG_2318', '', '');
          $gallery->addPopupImage('IMG_2321', '', '');
          $gallery->addPopupImage('IMG_2322', '', '');
          $gallery->addPopupImage('IMG_2324', '', '');
          $gallery->addPopupImage('IMG_2325', '', '');
          $gallery->addPopupImage('IMG_2329', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
