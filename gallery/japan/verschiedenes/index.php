<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Verschiedenes</h1>
        <p>
          Verschiedene Eindr&uuml;cke aus Japan.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('verschiedenes');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150411_172401', '', '');
          $gallery->addPopupImage('20150411_211524', '', '');
          $gallery->addPopupImage('20150413_143819', '', '');
          $gallery->addPopupImage('20150415_130903', '', '');
          $gallery->addPopupImage('IMG_2192', '', '');
          $gallery->addPopupImage('IMG_2206', '', '');
          $gallery->addPopupImage('IMG_2207', '', '');
          $gallery->addPopupImage('IMG_2405', '', '');
          $gallery->addPopupImage('IMG_2806', '', '');
          $gallery->addPopupImage('IMG_3074', '', '');
          $gallery->addPopupImage('IMG_3079', '', '');
          $gallery->addPopupImage('IMG_5766', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
