<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Anreise</h1>
        <p>
          So, Japan ich komme! Noch ein letztes deutsches Bier und dann geht es
          los. 11 Stunden Flug, viel Zeit f&uuml;r Essen und Herr der Ringe kann man
          auch alle Teile schauen.<br>
          Die Bahn ist &uuml;brigens genau so voll, wie das jeder behauptet. Das ist
          derma&szlig;en eng, da f&auml;llt man sofort auf als Ausl&auml;nder mit Koffer.
        </p>
        <p>
          F&uuml;r weitere Einblicke, einfach auf die Bilder klicken.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('anreise');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG-20150122-WA0001-1', '', '');
          $gallery->addPopupImage('IMG_1935', '', '');
          $gallery->addPopupImage('IMG_1937', '', '');
          $gallery->addPopupImage('IMG_1940', '', '');
          $gallery->addPopupImage('IMG_1942', '', '');
          $gallery->addPopupImage('IMG_1944', '', '');
          $gallery->addPopupImage('IMG_1946', '', '');
          $gallery->addPopupImage('IMG_1949', '', '');
          $gallery->addPopupImage('IMG_1953', '', '');
          $gallery->addPopupImage('IMG_1954', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
