<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Planlos durch die Stadt</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('planlos_durch_die_stadt');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2038', '', '');
          $gallery->addPopupImage('IMG_2042', '', '');
          $gallery->addPopupImage('IMG_2045', '', '');
          $gallery->addPopupImage('IMG_2049', '', '');
          $gallery->addPopupImage('IMG_2050', '', '');
          $gallery->addPopupImage('IMG_2051', '', '');
          $gallery->addPopupImage('IMG_2054', '', '');
          $gallery->addPopupImage('IMG_2059', '', '');
          $gallery->addPopupImage('IMG_2063', '', '');
          $gallery->addPopupImage('IMG_2064', '', '');
          $gallery->addPopupImage('IMG_2065', '', '');
          $gallery->addPopupImage('IMG_2077', '', '');
          $gallery->addPopupImage('IMG_2081', '', '');
          $gallery->addPopupImage('IMG_2087', '', '');
          $gallery->addPopupImage('IMG_2091', '', '');
          $gallery->addPopupImage('IMG_2097', '', '');
          $gallery->addPopupImage('IMG_2105', '', '');
          $gallery->addPopupImage('IMG_2106', '', '');
          $gallery->addPopupImage('IMG_2111', '', '');
          $gallery->addPopupImage('IMG_2115', '', '');
          $gallery->addPopupImage('IMG_2117', '', '');
          $gallery->addPopupImage('IMG_2120', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
