<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Fahrradpark</h1>
        <p>
          Ein sch&ouml;ner Park, der mit einer rie&szlig;igen Fahrradstrecke versehen ist.
          Also genau genommen ist der Park nur f&uuml;r zum Fahrrad fahren gebaut
          worden.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('fahrradpark');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('11058506_1429698164012892_860959601369376200_o', '', '');
          $gallery->addPopupImage('20150425_122132', '', '');
          $gallery->addPopupImage('20150425_124659', '', '');
          $gallery->addPopupImage('20150425_124801', '', '');
          $gallery->addPopupImage('20150425_124908', '', '');
          $gallery->addPopupImage('20150425_132401', '', '');
          $gallery->addPopupImage('20150425_133055', '', '');
          $gallery->addPopupImage('20150425_134131', '', '');
          $gallery->addPopupImage('20150425_134401', '', '');
          $gallery->addPopupImage('20150425_134557', '', '');
          $gallery->addPopupImage('20150425_134916', '', '');
          $gallery->addPopupImage('20150425_144212', '', '');
          $gallery->addPopupImage('20150425_144352', '', '');
          $gallery->addPopupImage('20150425_144720', '', '');
          $gallery->addPopupImage('20150425_144956', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
