<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>British Pub in Shibuya</h1>
        <p>
          Etwas Heimatgef&uuml;hl f&uuml;r unsere Engl&auml;nder. Wir waren zur Live&uuml;bertragung
          eines Australian Rugby Spieles da. Die Regeln habe ich bis jetzt
          noch nicht verstanden.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('british_pub_in_shibuya');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150527_191957', '', '');
          $gallery->addPopupImage('20150527_194256', '', '');
          $gallery->addPopupImage('20150527_200012', '', '');
          $gallery->addPopupImage('20150527_204531', '', '');
          $gallery->addPopupImage('20150527_214550', '', '');
          $gallery->addPopupImage('20150527_222302', '', '');
          $gallery->addPopupImage('20150527_225442', '', '');
          $gallery->addPopupImage('20150527_231944', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
