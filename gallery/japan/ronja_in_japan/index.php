<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Ronja in Japan</h1>
        <p>
          Ronja war in Japan, da sie mit ihrem Judo Verein ein Event hier
          besucht hat. Internationales Training in Tokyo.<br>
          Am abend sind wir dann mit der deutschen Judogruppe in die Stadt, um
          die Shibuya-Kreuzung zu sehen. Die Jungs haben dann auch direkt mal
          Liegest&uuml;tze gemacht und wurden zur Hauptattraktion.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('ronja_in_japan');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4890', '', '');
          $gallery->addPopupImage('IMG_4891', '', '');
          $gallery->addPopupImage('IMG_4893', '', '');
          $gallery->addPopupImage('IMG_4898', '', '');
          $gallery->addPopupImage('IMG_4900', '', '');
          $gallery->addPopupImage('IMG_4903', '', '');
          $gallery->addPopupImage('IMG_4904', '', '');
          $gallery->addPopupImage('IMG_4909', '', '');
          $gallery->addPopupImage('IMG_4914', '', '');
          $gallery->addPopupImage('IMG_4923', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
