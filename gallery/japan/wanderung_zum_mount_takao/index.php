<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Wanderung zum Mount Takao</h1>
        <p>
          Einige Eindr&uuml;cke unserer Wanderung zum Mount Takao und einem anderen,
          uns unbekannten, Berg. Dazwischen sind wir &uuml;ber diverse skurile und
          lustige Sachen gestolpert.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('wanderung_zum_mount_takao');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('P1010294', '', '');
          $gallery->addPopupImage('P1010298', '', '');
          $gallery->addPopupImage('P1010331', '', '');
          $gallery->addPopupImage('P1010335', '', '');
          $gallery->addPopupImage('P1010339', '', '');
          $gallery->addPopupImage('P1010349', '', '');
          $gallery->addPopupImage('P1010355', '', '');
          $gallery->addPopupImage('P1010360', '', '');
          $gallery->addPopupImage('P1010372', '', '');
          $gallery->addPopupImage('P1010373', '', '');
          $gallery->addPopupImage('P1010382', '', '');
          $gallery->addPopupImage('P1010391', '', '');
          $gallery->addPopupImage('P1010398', '', '');
          $gallery->addPopupImage('P1010417', '', '');
          $gallery->addPopupImage('P1010418', '', '');
          $gallery->addPopupImage('P1010424', '', '');
          $gallery->addPopupImage('P1010426', '', '');
          $gallery->addPopupImage('P1010438', '', '');
          $gallery->addPopupImage('P1010454', '', '');
          $gallery->addPopupImage('P1010458', '', '');
          $gallery->addPopupImage('P1010464', '', '');
          $gallery->addPopupImage('P1010468', '', '');
          $gallery->addPopupImage('P1010490', '', '');
          $gallery->addPopupImage('P1010500', '', '');
          $gallery->addPopupImage('P1010513', '', '');
          $gallery->addPopupImage('P1010515', '', '');
          $gallery->addPopupImage('P1010544', '', '');
          $gallery->addPopupImage('P1010548', '', '');
          $gallery->addPopupImage('P1010565', '', '');
          $gallery->addPopupImage('P1010577', '', '');
          $gallery->addPopupImage('P1010606', '', '');
          $gallery->addPopupImage('P1010613', '', '');
          $gallery->addPopupImage('P1010615', '', '');
          $gallery->addPopupImage('P1010642', '', '');
          $gallery->addPopupImage('IMG_2389', '', '');
          $gallery->addPopupImage('IMG_2376', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
