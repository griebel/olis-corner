<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Akihabara</h1>
        <p>
          Eine Tour durch "Electric City"<br>
          Die Spielkasinos sind einfach &uuml;bertrieben laut, Geh&ouml;rsch&auml;den
          garantiert. Allerdings ein muss!
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('akihabara');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150530_120941', '', '');
          $gallery->addPopupImage('20150530_124259', '', '');
          $gallery->addPopupImage('20150530_130050', '', '');
          $gallery->addPopupImage('20150530_133733', '', '');
          $gallery->addPopupImage('20150530_150226', '', '');
          $gallery->addPopupImage('20150530_153809', '', '');
          $gallery->addPopupImage('20150530_173118', '', '');
          $gallery->addPopupImage('20150530_174412', '', '');
          $gallery->addPopupImage('S__37224470', '', '');
          $gallery->addPopupImage('S__37224472', '', '');
          $gallery->addPopupImage('S__37224478', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
