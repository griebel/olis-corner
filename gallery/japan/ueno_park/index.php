<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Ueno Park</h1>
        <p>
          Rie&szlig;en Pandas im Zoo! Da musste ich einfach hin. Au&szlig;erdem auch noch
          extrem viel au&szlig;en rum. Nur Sonntags gehe ich nie wieder in den Zoo
          hier, total &uuml;berf&uuml;llt.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('ueno_park');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2882', '', '');
          $gallery->addPopupImage('IMG_2884', '', '');
          $gallery->addPopupImage('IMG_2886', '', '');
          $gallery->addPopupImage('IMG_2891', '', '');
          $gallery->addPopupImage('IMG_2892', '', '');
          $gallery->addPopupImage('IMG_2897', '', '');
          $gallery->addPopupImage('IMG_2900', '', '');
          $gallery->addPopupImage('IMG_2905', '', '');
          $gallery->addPopupImage('IMG_2912', '', '');
          $gallery->addPopupImage('IMG_2921', '', '');
          $gallery->addPopupImage('IMG_2928', '', '');
          $gallery->addPopupImage('IMG_2930', '', '');
          $gallery->addPopupImage('IMG_2933', '', '');
          $gallery->addPopupImage('IMG_2934', '', '');
          $gallery->addPopupImage('IMG_2941', '', '');
          $gallery->addPopupImage('IMG_2946', '', '');
          $gallery->addPopupImage('IMG_2949', '', '');
          $gallery->addPopupImage('IMG_2952', '', '');
          $gallery->addPopupImage('IMG_2955', '', '');
          $gallery->addPopupImage('IMG_2957', '', '');
          $gallery->addPopupImage('IMG_2960', '', '');
          $gallery->addPopupImage('IMG_2967', '', '');
          $gallery->addPopupImage('IMG_2968', '', '');
          $gallery->addPopupImage('IMG_2973', '', '');
          $gallery->addPopupImage('IMG_2977', '', '');
          $gallery->addPopupImage('IMG_2980', '', '');
          $gallery->addPopupImage('IMG_2981', '', '');
          $gallery->addPopupImage('IMG_2984', '', '');
          $gallery->addPopupImage('IMG_2986', '', '');
          $gallery->addPopupImage('IMG_2988', '', '');
          $gallery->addPopupImage('IMG_2994', '', '');
          $gallery->addPopupImage('IMG_3002', '', '');
          $gallery->addPopupImage('IMG_3009', '', '');
          $gallery->addPopupImage('IMG_3027', '', '');
          $gallery->addPopupImage('IMG_3029', '', '');
          $gallery->addPopupImage('IMG_3034', '', '');
          $gallery->addPopupImage('IMG_3035', '', '');
          $gallery->addPopupImage('IMG_3037', '', '');
          $gallery->addPopupImage('IMG_3039', '', '');
          $gallery->addPopupImage('IMG_3041', '', '');
          $gallery->addPopupImage('IMG_3048', '', '');
          $gallery->addPopupImage('IMG_3049', '', '');
          $gallery->addPopupImage('IMG_3057', '', '');
          $gallery->addPopupImage('IMG_3062', '', '');
          $gallery->addPopupImage('IMG_3063', '', '');
          $gallery->addPopupImage('IMG_3065', '', '');
          $gallery->addPopupImage('IMG_3069', '', '');
          $gallery->addPopupImage('IMG_3071', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
