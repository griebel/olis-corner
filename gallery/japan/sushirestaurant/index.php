<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Sushirestaurant</h1>
        <p>
          Das erste mal original japanisch Sushi essen, direkt vom Band.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('sushirestaurant');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150421_182659', '', '');
          $gallery->addPopupImage('20150421_183235', '', '');
          $gallery->addPopupImage('20150421_183428', '', '');
          $gallery->addPopupImage('20150421_183855', '', '');
          $gallery->addPopupImage('20150421_183929', '', '');
          $gallery->addPopupImage('20150421_184248', '', '');
          $gallery->addPopupImage('20150421_185516', '', '');
          $gallery->addPopupImage('20150421_185847', '', '');
          $gallery->addPopupImage('20150421_190213', '', '');
          $gallery->addPopupImage('20150421_190648', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
