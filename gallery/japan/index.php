<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Japan 2015</h1>
        <p>
          Auslandssemester in Tokyo/Japan im Sommer 2015.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery('anreise');
          // $gallery->addClickableImage($folder, $filename, $title, $text)
          $gallery->addClickableImage('anreise', 'IMG_1954', 'Anreise', '');
          $gallery->addClickableImage('meine_unterkunft', 'IMG_1961', 'Meine Unterkunft', '');
          $gallery->addClickableImage('der_erste_tag', 'IMG_1960', 'Der erste Tag', '');
          $gallery->addClickableImage('uni_tour', 'IMG_1987', 'Uni Tour', '');
          $gallery->addClickableImage('anmeldung_in_tokyo', 'IMG_2139', 'Anmeldung in Tokyo', '');
          $gallery->addClickableImage('planlos_durch_die_stadt', 'IMG_2087', 'Planlos durch die Stadt', '');
          $gallery->addClickableImage('shibuya_und_der_meiji_schrein', 'IMG_2222', 'Shibuya und der Meiji-Schrein', '');
          $gallery->addClickableImage('hanami_vor_der_haustuer', 'IMG_2367', 'Hanami vor der Haust&uuml;r', '');
          $gallery->addClickableImage('wanderung_zum_mount_takao', 'P1010565', 'Wanderung zum Mount Takao', '');
          $gallery->addClickableImage('sushirestaurant', '20150421_190648', 'Sushirestaurant', '');
          $gallery->addClickableImage('fahrradpark', '11058506_1429698164012892_860959601369376200_o', 'Fahrradpark', '');
          $gallery->addClickableImage('fahrradtour_durch_westjapan', 'P1010702', 'Fahrradtour durch Westjapan', '');
          $gallery->addClickableImage('thai_food_festival', 'IMG_2741', 'Thai Food Festival &amp; Zombiewalk', '');
          $gallery->addClickableImage('ikea', '20150518_171748', 'IKEA', '');
          $gallery->addClickableImage('sumo_turnier', 'P1010727', 'Sumo Turnier', '');
          $gallery->addClickableImage('shabu_shabu', '20150522_192736', 'Shabu-Shabu', '');
          $gallery->addClickableImage('akihabara', '20150530_124259', 'Akihabara', '');
          $gallery->addClickableImage('thailaendisches_restaurant', '20150604_183128', 'Thail&auml;ndisches Restaurant', '');
          $gallery->addClickableImage('ueno_park', 'IMG_2891', 'Ueno Park', '');
          $gallery->addClickableImage('fuji_q_highland_park', 'IMG_3140', 'FUJI-Q Highland Park', '');
          $gallery->addClickableImage('einkaufsbummel', 'IMG_3174', 'Einkaufsbummel', '');
          $gallery->addClickableImage('okonomiyaki_restaurant', '20150525_201847', 'Okonomiyaki Restaurant', '');
          $gallery->addClickableImage('british_pub_in_shibuya', '20150527_200012', 'British Pub in Shibuya', '');
          $gallery->addClickableImage('jonas_22ter', 'IMG_2860', 'Jonas 22ter', '');
          $gallery->addClickableImage('hortensienbluete_in_kamakura', 'IMG_3249', 'Hortensienbl&uuml;te in Kamakura', '');
          $gallery->addClickableImage('mount_fuji', 'IMG_3560', 'Mount Fuji', '');
          $gallery->addClickableImage('ronja_in_japan', 'IMG_4904', 'Ronja in Japan', '');
          $gallery->addClickableImage('echtes_sushi', '20150802_011524', 'Echtes Sushi', '');
          $gallery->addClickableImage('media_cafe_popeye', 'IMG_5050', 'Media Cafe Popeye', '');
          $gallery->addClickableImage('tokyo_metropolitan_university', '20150512_101014', 'Tokyo Metropolitan University (TMU)', '');
          $gallery->addClickableImage('essen_und_trinken', 'IMG_3073', 'Essen &amp; Trinken', '');
          $gallery->addClickableImage('verschiedenes', '20150411_172401', 'Verschiedenes', '');
          ?>
        </ul>
        <p>
          F&uuml;r weitere Einblicke auf die Bilder klicken.
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
</body>
</html>
