<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Sumo Turnier</h1>
        <p>
          Mein erstes live Sumo Turnier. Nur echt in Japan.<br>
          Die Kollegen sind schon ziemliche Tr&uuml;mmer.<br>
          Zwischendurch haben wir einen Park besucht, da wir morgens um 8 Uhr
          schon die Karten holen mussten. Los ging es aber erst am Nachmittag.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('sumo_turnier');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_2783', '', '');
          $gallery->addPopupImage('IMG_2785', '', '');
          $gallery->addPopupImage('IMG_2805', '', '');
          $gallery->addPopupImage('IMG_2808', '', '');
          $gallery->addPopupImage('IMG_2810', '', '');
          $gallery->addPopupImage('IMG_2811', '', '');
          $gallery->addPopupImage('IMG_2817', '', '');
          $gallery->addPopupImage('IMG_2820', '', '');
          $gallery->addPopupImage('IMG_2822', '', '');
          $gallery->addPopupImage('20150522_3262', '', '');
          $gallery->addPopupImage('P1010716', '', '');
          $gallery->addPopupImage('P1010727', '', '');
          $gallery->addPopupImage('P1010733', '', '');
          $gallery->addPopupImage('P1010740', '', '');
          $gallery->addPopupImage('P1010742', '', '');
          $gallery->addPopupImage('P1010776', '', '');
          $gallery->addPopupImage('P1010786', '', '');
          $gallery->addPopupImage('P1010788', '', '');
          $gallery->addPopupImage('P1010801', '', '');
          $gallery->addPopupImage('P1010803', '', '');
          $gallery->addPopupImage('P1010835', '', '');
          $gallery->addPopupImage('P1010839', '', '');
          $gallery->addPopupImage('20150522_3449', '', '');
          $gallery->addPopupImage('IMG_2824', '', '');
          $gallery->addPopupImage('20150522_3352', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
