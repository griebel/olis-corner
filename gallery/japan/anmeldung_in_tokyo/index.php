<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_japan.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Anmeldung in Tokyo (<ruby>東<rp>(</rp><rt>とう</rt>
          <rp>)</rp>京<rp>(</rp><rt>きょう</rt><rp>)</rp></ruby>)</h1>
          <p>
            Selbst die deutsche B&uuml;rokratie ist ein Witz dagegen, was hier geboten
            wird. Ich werde mich nie wieder beschweren.
          </p>
          <ul class="rig columns-3">
            <?php
            $gallery = new Gallery();
            $gallery->setName('anmeldung_in_tokyo');
            // $gallery->addPopupImage($filename, $title, $text);
            $gallery->addPopupImage('IMG_2137', '', '');
            $gallery->addPopupImage('IMG_2139', '', '');
            $gallery->addPopupImage('IMG_2141', '', '');
            $gallery->addPopupImage('IMG_2142', '', '');
            $gallery->addPopupImage('IMG_2145', '', '');
            $gallery->addPopupImage('IMG_2149', '', '');
            $gallery->addPopupImage('IMG_2152', '', '');
            ?>
          </ul>
          <p>
            <a href="..">Zur&uuml;ck</a>
          </p>
        </div>
        <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
  </body>
  </html>
