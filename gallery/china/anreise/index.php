<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Anreise</h1>
        <p>
          Anreise aus Japan, Tokio.<br>
          Narita Airport ist eine absolut interessante Sache, besonders da es wesentlich mehr Sicherheitskontrollen sind wie in Deutschland. Zum Gl&uuml;ck bin ich Donnerstags geflogen, das Flugzeug war nicht einmal zur H&auml;lfte gef&uuml;llt.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('anreise');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150708_153930', '', '');
          $gallery->addPopupImage('IMG_3599', '', '');
          $gallery->addPopupImage('IMG_3623', '', '');
          $gallery->addPopupImage('IMG_3624', '', '');
          $gallery->addPopupImage('IMG_3628', '', '');
          $gallery->addPopupImage('IMG_3634', '', '');
          $gallery->addPopupImage('IMG_3635', '', '');
          $gallery->addPopupImage('IMG_3638', '', '');
          $gallery->addPopupImage('IMG_3642', '', '');
          $gallery->addPopupImage('IMG_3651', '', '');
          $gallery->addPopupImage('IMG_3654', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
