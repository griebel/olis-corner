<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>China 2015</h1>
        <p>
          Eine Woche in Peking/China im Juli 2015.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          // $gallery->addClickableImage($folder, $filename, $title, $text);
          $gallery->addClickableImage('anreise', 'IMG_3623', 'Anreise', '');
          $gallery->addClickableImage('meine_unterkunft', 'IMG_3662', 'Meine Unterkunft', '');
          $gallery->addClickableImage('die_hutongs', 'IMG_3998', 'Die Hutons', '');
          $gallery->addClickableImage('die_verbotene_stadt', 'IMG_3698', 'Die verbotene Stadt', '');
          $gallery->addClickableImage('jingshan_park', 'IMG_3962', 'Jingshan Park', '');
          $gallery->addClickableImage('chinesische_mauer', 'IMG_4134', 'Chinesische Mauer', '');
          $gallery->addClickableImage('stadtbummel', 'IMG_4497', 'Stadtbummel', '');
          $gallery->addClickableImage('bagou_shanshui_park', 'IMG_4281', 'Bagou Shanshui Park', '');
          $gallery->addClickableImage('sommer_palast', 'IMG_4306', 'Sommer Palast', '');
          $gallery->addClickableImage('shishahai_bar_street', 'IMG_4067', 'Shishahai Bar Street',  '');
          $gallery->addClickableImage('lama_tempel', '20150712_113043', 'Lama Tempel', '');
          $gallery->addClickableImage('drum_tower', 'IMG_4453', 'Drum Tower', '');
          $gallery->addClickableImage('nachtmarkt', 'IMG_4412', 'Nachtmarkt', '');
          $gallery->addClickableImage('temple_of_heaven', 'IMG_4526', 'Temple of Heaven', '');
          $gallery->addClickableImage('olympia_stadion', 'IMG_4089', 'Olympia Stadion', '');
          $gallery->addClickableImage('essen_und_trinken', 'IMG_3676', 'Essen &amp; Trinken', '');
          ?>
        </ul>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
</body>
</html>
