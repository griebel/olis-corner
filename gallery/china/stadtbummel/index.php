﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Stadtbummel</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('stadtbummel');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4422', '', '');
          $gallery->addPopupImage('IMG_4424', '', '');
          $gallery->addPopupImage('IMG_4469', '', '');
          $gallery->addPopupImage('IMG_4472', '', '');
          $gallery->addPopupImage('IMG_4480', '', '');
          $gallery->addPopupImage('IMG_4483', '', '');
          $gallery->addPopupImage('IMG_4491', '', '');
          $gallery->addPopupImage('IMG_4492', '', '');
          $gallery->addPopupImage('IMG_4494', '', '');
          $gallery->addPopupImage('IMG_4495', '', '');
          $gallery->addPopupImage('IMG_4496', '', '');
          $gallery->addPopupImage('IMG_4497', '', '');
          $gallery->addPopupImage('IMG_4500', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
