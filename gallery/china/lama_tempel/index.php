﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Lama Tempel</h1>
        <p>
          Der Lama Tempel oder auch Palast des Friedens und der Harmonie. War einst Palast eines Prinzen und wurde dann zu ehren eines Kaisers 17xx in einen Tempel umgebaut. Hier wurde mir erst so richtig klar, wie streng gl&auml;ubig manche Chinesen sind. Au&szlig;erdem wusste ich nicht, dass man so viele R&auml;ucherst&auml;bchen auf einmal verbrennen kann. Von weitem sah es aus wie ein Brand.<br>
          Leider nicht die beste Bildqualit&auml;t, da ich meine Kamera vergessen habe.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('lama_tempel');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('20150712_113043', '', '');
          $gallery->addPopupImage('20150712_113528', '', '');
          $gallery->addPopupImage('20150712_113556', '', '');
          $gallery->addPopupImage('20150712_113617', '', '');
          $gallery->addPopupImage('20150712_113906', '', '');
          $gallery->addPopupImage('20150712_114016', '', '');
          $gallery->addPopupImage('20150712_114413', '', '');
          $gallery->addPopupImage('20150712_114702', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
