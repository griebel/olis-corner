﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Jingshan Park</h1>
        <p>
          Rund um einen H&uuml;gel im Stadtzentrum Pekings befindet sich der Jingshan Park, welcher eine wunderbare aussicht auf die Verbotene Stadt und die komplette Umgebung gew&auml;hrt.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('jingshan_park');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3947', '', '');
          $gallery->addPopupImage('IMG_3950', '', '');
          $gallery->addPopupImage('IMG_3958', '', '');
          $gallery->addPopupImage('IMG_3962', '', '');
          $gallery->addPopupImage('IMG_3968', '', '');
          $gallery->addPopupImage('IMG_3976', '', '');
          $gallery->addPopupImage('IMG_3978', '', '');
          $gallery->addPopupImage('IMG_3982', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
