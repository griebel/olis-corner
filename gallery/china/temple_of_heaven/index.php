﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Temple of Heaven</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('temple_of_heaven');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4517', '', '');
          $gallery->addPopupImage('IMG_4524', '', '');
          $gallery->addPopupImage('IMG_4526', '', '');
          $gallery->addPopupImage('IMG_4558', '', '');
          $gallery->addPopupImage('IMG_4561', '', '');
          $gallery->addPopupImage('IMG_4566', '', '');
          $gallery->addPopupImage('IMG_4573', '', '');
          $gallery->addPopupImage('IMG_4580', '', '');
          $gallery->addPopupImage('IMG_4582', '', '');
          $gallery->addPopupImage('IMG_4593', '', '');
          $gallery->addPopupImage('IMG_4597', '', '');
          $gallery->addPopupImage('IMG_4598', '', '');
          $gallery->addPopupImage('IMG_4604', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
