﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Chinesische Mauer</h1>
        <p>
          Wanderung auf einem Teilabschnitt der Chinesischen Mauer in Jinshanling, 140km au&szlig;erhalb vom Peking. Ich hatte ein Programm mit Busfahrt und Verpflegung gebucht, welches sich allein durch das Mittagessen gelohnt hat. Aufgrund &uuml;bergro&szlig;en Hungers gibt es davon aber leider keine Bilder.<br>
          Ich habe nur einen winzig kleinen Bruchteil der Mauer gesehen. 7000km sind nun mal eben nicht so zur&uuml;ckzulegen. Allerdings war dieser kleine Part schon extrem beeindruckend. Mein pers&ouml;nliches Highlight hier in China bisher und wird es wahrscheinlich auch bleiben.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('chinesische_mauer');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4112', '', '');
          $gallery->addPopupImage('IMG_4116', '', '');
          $gallery->addPopupImage('IMG_4131', '', '');
          $gallery->addPopupImage('IMG_4133', '', '');
          $gallery->addPopupImage('IMG_4134', '', '');
          $gallery->addPopupImage('IMG_4136', '', '');
          $gallery->addPopupImage('IMG_4144', '', '');
          $gallery->addPopupImage('IMG_4150', '', '');
          $gallery->addPopupImage('IMG_4153', '', '');
          $gallery->addPopupImage('IMG_4158', '', '');
          $gallery->addPopupImage('IMG_4166', '', '');
          $gallery->addPopupImage('IMG_4176', '', '');
          $gallery->addPopupImage('IMG_4179', '', '');
          $gallery->addPopupImage('IMG_4183', '', '');
          $gallery->addPopupImage('IMG_4185', '', '');
          $gallery->addPopupImage('IMG_4201', '', '');
          $gallery->addPopupImage('IMG_4213', '', '');
          $gallery->addPopupImage('IMG_4218', '', '');
          $gallery->addPopupImage('IMG_4220', '', '');
          $gallery->addPopupImage('IMG_4244', '', '');
          $gallery->addPopupImage('IMG_4245', '', '');
          $gallery->addPopupImage('IMG_4262', '', '');
          $gallery->addPopupImage('IMG_4265', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
