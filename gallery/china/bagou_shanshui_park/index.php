<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Bagou Shanshui Park</h1>
        <p>
          Ein kleiner Park in der n&auml;he des Sommer Palastes. Ich fand die Bemahlung der W&auml;nde sehr beeindruckend und fantasievoll.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('bagou_shanshui_park');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4276', '', '');
          $gallery->addPopupImage('IMG_4281', '', '');
          $gallery->addPopupImage('IMG_4282', '', '');
          $gallery->addPopupImage('IMG_4283', '', '');
          $gallery->addPopupImage('IMG_4284', '', '');
          $gallery->addPopupImage('IMG_4287', '', '');
          $gallery->addPopupImage('IMG_4291', '', '');
          $gallery->addPopupImage('IMG_4294', '', '');
          $gallery->addPopupImage('IMG_4297', '', '');
          $gallery->addPopupImage('IMG_4298', '', '');
          $gallery->addPopupImage('IMG_4300', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
