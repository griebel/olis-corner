﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Sommer Palast</h1>
        <p>
          Der Sommer Palast in Peking ist extrem weitl&auml;ufig. Nach Betreten im S&uuml;deingang und &uuml;ber 3 Stunden Marsch an den Nordeingang, habe ich ihn auch dort verlassen.<br>
          Das beste war, dass ich dort eine Rentergruppe getroffen habe die bei ihren morgentlichen &uuml;bungen waren. Diese besteht aus einem Hacky Sack &auml;hnlichen Spiel, wobei der Ball eher einem Federball gleicht der mit Gewichten beschwert ist. Gemeinsam haben wir so spielend eine geraume Zeit verbracht und die &uuml;ber 60 Jahre alten Herrschaften haben mich in der Hitze alt aussehen lassen.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('sommer_palast');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4306', '', '');
          $gallery->addPopupImage('IMG_4312', '', '');
          $gallery->addPopupImage('IMG_4313', '', '');
          $gallery->addPopupImage('IMG_4319', '', '');
          $gallery->addPopupImage('IMG_4322', '', '');
          $gallery->addPopupImage('IMG_4326', '', '');
          $gallery->addPopupImage('IMG_4330', '', '');
          $gallery->addPopupImage('IMG_4340', '', '');
          $gallery->addPopupImage('IMG_4346', '', '');
          $gallery->addPopupImage('IMG_4347', '', '');
          $gallery->addPopupImage('IMG_4348', '', '');
          $gallery->addPopupImage('IMG_4350', '', '');
          $gallery->addPopupImage('IMG_4354', '', '');
          $gallery->addPopupImage('IMG_4357', '', '');
          $gallery->addPopupImage('IMG_4359', '', '');
          $gallery->addPopupImage('IMG_4361', '', '');
          $gallery->addPopupImage('IMG_4368', '', '');
          $gallery->addPopupImage('IMG_4370', '', '');
          $gallery->addPopupImage('IMG_4371', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
