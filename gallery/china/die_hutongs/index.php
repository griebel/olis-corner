﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Die Hutongs</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('die_hutongs');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3666', '', '');
          $gallery->addPopupImage('IMG_3667', '', '');
          $gallery->addPopupImage('IMG_3986', '', '');
          $gallery->addPopupImage('IMG_3987', '', '');
          $gallery->addPopupImage('IMG_3989', '', '');
          $gallery->addPopupImage('IMG_3995', '', '');
          $gallery->addPopupImage('IMG_3998', '', '');
          $gallery->addPopupImage('IMG_4000', '', '');
          $gallery->addPopupImage('IMG_4001', '', '');
          $gallery->addPopupImage('IMG_4009', '', '');
          $gallery->addPopupImage('IMG_4010', '', '');
          $gallery->addPopupImage('IMG_4016', '', '');
          $gallery->addPopupImage('IMG_4024', '', '');
          $gallery->addPopupImage('IMG_4027', '', '');
          $gallery->addPopupImage('IMG_4037', '', '');
          $gallery->addPopupImage('IMG_4038', '', '');
          $gallery->addPopupImage('IMG_4043', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
