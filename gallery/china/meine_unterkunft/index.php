﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Meine Unterkunft</h1>
        <p>
          Meine Unterkunft im Zentrum Pekings. In den Hutons nahe des Herrscherpalastes und der verbotenen Stadt.<br>
          Ich liebe unseren Gemeinschaftsraum und der Besitzer bezeichnet alle im Haus als gro&szlig;e Familie. Und genau so f&uuml;hlt man sich auch. Ebenfalls super ist das 8 Bett Zimmer, in welchem ich schlafe. 4 Betten im Erdgeschoss und 4 Betten in zwei Zimmern im Obergeschoss. Da keine T&uuml;r vorhanden ist und nur eine Gemeinschaftsdusche und Toilette besteht ist das wohl der Grund f&uuml;r diese Bezeichnung.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('meine_unterkunft');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3658', '', '');
          $gallery->addPopupImage('IMG_3659', '', '');
          $gallery->addPopupImage('IMG_3660', '', '');
          $gallery->addPopupImage('IMG_3661', '', '');
          $gallery->addPopupImage('IMG_3662', '', '');
          $gallery->addPopupImage('IMG_3663', '', '');
          $gallery->addPopupImage('IMG_3664', '', '');
          $gallery->addPopupImage('IMG_4381', '', '');
          $gallery->addPopupImage('IMG_4384', '', '');
          $gallery->addPopupImage('IMG_4388', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
