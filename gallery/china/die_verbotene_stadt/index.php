﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Die verbotene Stadt</h1>
        <p>
          Die Verbotene Stadt steht mitten im Zentrum Pekings und ist eine absolute Sehensw&uuml;rdigkeit. Hierbei handelt es sich jedoch weniger um einen Stadtteil als mehr um einen Palast. Wundersch&ouml;ne alte Architektur und verschiedene Museen.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('die_verbotene_stadt');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3681', '', '');
          $gallery->addPopupImage('IMG_3698', '', '');
          $gallery->addPopupImage('IMG_3704', '', '');
          $gallery->addPopupImage('IMG_3711', '', '');
          $gallery->addPopupImage('IMG_3734', '', '');
          $gallery->addPopupImage('IMG_3736', '', '');
          $gallery->addPopupImage('IMG_3741', '', '');
          $gallery->addPopupImage('IMG_3743', '', '');
          $gallery->addPopupImage('IMG_3755', '', '');
          $gallery->addPopupImage('IMG_3760', '', '');
          $gallery->addPopupImage('IMG_3770', '', '');
          $gallery->addPopupImage('IMG_3819', '', '');
          $gallery->addPopupImage('IMG_3835', '', '');
          $gallery->addPopupImage('IMG_3840', '', '');
          $gallery->addPopupImage('IMG_3842', '', '');
          $gallery->addPopupImage('IMG_3858', '', '');
          $gallery->addPopupImage('IMG_3908', '', '');
          $gallery->addPopupImage('IMG_3911', '', '');
          $gallery->addPopupImage('IMG_3914', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
