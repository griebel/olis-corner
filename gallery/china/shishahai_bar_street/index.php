﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Shishahai Bar Street</h1>
        <p>
          Einmal rund um den See gibt es hier essen, trinken und gut gelaunte Leute. Alle 'zig Meter gibt es ein Konzert umsonst mit den verschiedensten Musikarten.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('shishahai_bar_street');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4052', '', '');
          $gallery->addPopupImage('IMG_4053', '', '');
          $gallery->addPopupImage('IMG_4054', '', '');
          $gallery->addPopupImage('IMG_4060', '', '');
          $gallery->addPopupImage('IMG_4067', '', '');
          $gallery->addPopupImage('IMG_4070', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
