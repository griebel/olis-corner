﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Essen &amp; Trinken</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('essen_und_trinken');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_3657', '', '');
          $gallery->addPopupImage('IMG_3676', '', '');
          $gallery->addPopupImage('IMG_3949', '', '');
          $gallery->addPopupImage('IMG_4004', '', '');
          $gallery->addPopupImage('IMG_4048', '', '');
          $gallery->addPopupImage('IMG_4055', '', '');
          $gallery->addPopupImage('IMG_4272', '', '');
          $gallery->addPopupImage('IMG_4273', '', '');
          $gallery->addPopupImage('IMG_4336', '', '');
          $gallery->addPopupImage('IMG_4337', '', '');
          $gallery->addPopupImage('IMG_4338', '', '');
          $gallery->addPopupImage('IMG_4342', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
