﻿<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_china.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Nachtmarkt</h1>
        <p>
          Der Nachtmarkt mit allerlei leckerei und skurilem. Hier bekommt man alles, nur nichts bekanntes. Tintenfisch, Oktopus, Krabbe usw. Alles wird gegrillt, in &ouml;l gebadet oder fritiert.<br>
          Ich habe hier mein must do f&uuml;r China erledigt, essen von Spinne, Schlange und Skorpion. Ich kann euch sagen, eine wahnsinns erfahrung!
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('nachtmarkt');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_4397', '', '');
          $gallery->addPopupImage('IMG_4398', '', '');
          $gallery->addPopupImage('IMG_4399', '', '');
          $gallery->addPopupImage('IMG_4400', '', '');
          $gallery->addPopupImage('IMG_4401', '', '');
          $gallery->addPopupImage('IMG_4405', '', '');
          $gallery->addPopupImage('IMG_4407', '', '');
          $gallery->addPopupImage('IMG_4408', '', '');
          $gallery->addPopupImage('IMG_4412', '', '');
          $gallery->addPopupImage('IMG_4413', '', '');
          $gallery->addPopupImage('IMG_4414', '', '');
          $gallery->addPopupImage('IMG_4415', '', '');
          $gallery->addPopupImage('IMG_4417', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
