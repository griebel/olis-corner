<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Cambridge</h1>
        <p>
          "Cycle city" wird Cambridge nicht ohne Grund genannt. Es ist flach und
          alles zentral gelegen. Man erreicht fast alles innerhalb von 20min,
          was sich mit dem Auto nicht selten als schwieriger erweisen w&uuml;rde.
          Feierabendverkehr interessiert den Radfahrer ebenfalls herzlich wenig
          und die Bewegung gibt es gratis dazu.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('cambridge');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_6835', 'Kinder-Halloween', '');
          $gallery->addPopupImage('IMG_6838', 'Kings Hedges', '');
          $gallery->addPopupImage('IMG_6845', 'Sonnenaufgang', '');
          $gallery->addPopupImage('IMG_6849', 'Herbst', '');
          $gallery->addPopupImage('IMG_6852', 'Reihenh&auml;user', '');
          $gallery->addPopupImage('IMG_6857', 'Gassen', '');
          $gallery->addPopupImage('IMG_6858', 'Cambridge', '');
          $gallery->addPopupImage('IMG_6860', 'Cycle city', '');
          $gallery->addPopupImage('IMG_6863', 'Kings college', '');
          $gallery->addPopupImage('IMG_6869', 'Stadtpark', '');
          $gallery->addPopupImage('IMG_6874', 'Fluss &quot;Cam&quot;', '');
          $gallery->addPopupImage('IMG_6876', 'Stadtzentrum', '');
          $gallery->addPopupImage('IMG_7163', '', '');
          $gallery->addPopupImage('IMG_7178', '', '');
          $gallery->addPopupImage('IMG_7185', '', '');
          $gallery->addPopupImage('IMG_7197', '', '');
          $gallery->addPopupImage('IMG_7198', '', '');
          $gallery->addPopupImage('IMG_7236', '', '');
          $gallery->addPopupImage('IMG_7271', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
