<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>England 2016</h1>
        <p>
          Meine Sprachreise nach Cambridge im Herbst 2016. Zu 4 Wochen
          englischer Kultur und K&uuml;che gibt es hier ein paar kleine
          Eindr&uuml;cke.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          // $gallery->addClickableImage($folder, $filename, $title, $text);
          $gallery->addClickableImage('anreise', 'IMG_6814', 'Anreise', '');
          $gallery->addClickableImage('cambridge', 'IMG_6860', 'Cambridge', '');
          $gallery->addClickableImage('kings_college', 'IMG_7200', 'Kings College', '');
          $gallery->addClickableImage('embassy_school', 'IMG_7164', 'Embassy School', '');
          $gallery->addClickableImage('norwich', 'IMG_6884', 'Norwich', '');
          $gallery->addClickableImage('bury_st_edmunds', 'IMG_6954', 'Bury St Edmunds', '');
          $gallery->addClickableImage('warwick_castle', 'IMG_6983', 'Warwick Castle', '');
          $gallery->addClickableImage('bonfire_night', 'IMG_7239', 'Bonfire Night', '');
          $gallery->addClickableImage('oxford', 'IMG_7276', 'Oxford', '');
          ?>
        </ul>
        <p>
          F&uuml;r weitere Einblicke auf die Bilder klicken.
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
</body>
</html>
