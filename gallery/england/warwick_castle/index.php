<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Warwick Castle</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('warwick_castle');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_6983', '', '');
          $gallery->addPopupImage('IMG_6986', '', '');
          $gallery->addPopupImage('IMG_6998', '', '');
          $gallery->addPopupImage('IMG_7001', '', '');
          $gallery->addPopupImage('IMG_7010', '', '');
          $gallery->addPopupImage('IMG_7012', '', '');
          $gallery->addPopupImage('IMG_7019', '', '');
          $gallery->addPopupImage('IMG_7022', '', '');
          $gallery->addPopupImage('IMG_7042', '', '');
          $gallery->addPopupImage('IMG_7044', '', '');
          $gallery->addPopupImage('IMG_7046', '', '');
          $gallery->addPopupImage('IMG_7047', '', '');
          $gallery->addPopupImage('IMG_7050', '', '');
          $gallery->addPopupImage('IMG_7063', '', '');
          $gallery->addPopupImage('IMG_7094', '', '');
          $gallery->addPopupImage('IMG_7113', '', '');
          $gallery->addPopupImage('IMG_7117', '', '');
          $gallery->addPopupImage('IMG_7120', '', '');
          $gallery->addPopupImage('IMG_7121', '', '');
          $gallery->addPopupImage('IMG_7134', '', '');
          $gallery->addPopupImage('IMG_7137', '', '');
          $gallery->addPopupImage('IMG_7138', '', '');
          $gallery->addPopupImage('IMG_7149', '', '');
          $gallery->addPopupImage('IMG_7155', '', '');
          $gallery->addPopupImage('IMG_7159', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
