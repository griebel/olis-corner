<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Norwich</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('norwich');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_6884', '', '');
          $gallery->addPopupImage('IMG_6886', '', '');
          $gallery->addPopupImage('IMG_6888', '', '');
          $gallery->addPopupImage('IMG_6890', '', '');
          $gallery->addPopupImage('IMG_6892', '', '');
          $gallery->addPopupImage('IMG_6895', '', '');
          $gallery->addPopupImage('IMG_6898', '', '');
          $gallery->addPopupImage('IMG_6902', '', '');
          $gallery->addPopupImage('IMG_6903', '', '');
          $gallery->addPopupImage('IMG_6906', '', '');
          $gallery->addPopupImage('IMG_6911', '', '');
          $gallery->addPopupImage('IMG_6921', '', '');
          $gallery->addPopupImage('IMG_6924', '', '');
          $gallery->addPopupImage('IMG_6930', '', '');
          $gallery->addPopupImage('IMG_6932', '', '');
          $gallery->addPopupImage('IMG_6933', '', '');
          $gallery->addPopupImage('IMG_6934', '', '');
          $gallery->addPopupImage('IMG_6944', '', '');
          $gallery->addPopupImage('IMG_6946', '', '');
          $gallery->addPopupImage('IMG_6951', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
