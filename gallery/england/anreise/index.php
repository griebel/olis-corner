<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Anreise</h1>
        <p>
          Mit der Lacrosse-Ausr&uuml;stung w&auml;re Fliegen recht teuer gewesen.
          Au&szlig;erdem, was gibt es besseres als flexibel in einem fremden Land zu
          sein?<br>
          Also los geht's mit dem auf die Insel, abfahrt Sonntag morgens um
          4:00 Uhr nach Calais, Frankreich. Von dort nach Dover, England mit der
          Autof&auml;hre und &uuml;ber London nach Cambridge. Angekommen bin ich
          schlie&szlig;lich um 16:00 Uhr Ortszeit. Empfangen hat mich eine &auml;ltere
          Dame mit ihrem Gatten. Zwar sind die beiden in Rente, aber dennoch
          sind sie bis heute jung geblieben und haben immer einen lockeren
          Spruch auf der Zunge. Das einzig ern&uuml;chternde nach diesem
          erm&uuml;denden Tag, ich glaube ich habe soeben das kleinste
          Schlafzimmer auf Erden gefunden.
        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('anreise');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_6765', 'Warten auf die F&auml;hre', '');
          $gallery->addPopupImage('IMG_6767', 'Ablegen', '');
          $gallery->addPopupImage('IMG_6768', 'Erstes englisches Fr&uuml;hst&uuml;ck', '');
          $gallery->addPopupImage('IMG_6806', 'Blinder Passagier', '');
          $gallery->addPopupImage('IMG_6811', 'Blinder Passagier 2', '');
          $gallery->addPopupImage('IMG_6814', 'Lumberjacks on tour', '');
          $gallery->addPopupImage('IMG_6830', 'Mein Reich', '');
          $gallery->addPopupImage('IMG_6832', 'Der W&auml;schebeutel', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
