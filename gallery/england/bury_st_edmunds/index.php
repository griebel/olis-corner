<!DOCTYPE html>
<html>
<head>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_gallery.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_gallery_england.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Bury St Edmunds</h1>
        <p>

        </p>
        <ul class="rig columns-3">
          <?php
          $gallery = new Gallery();
          $gallery->setName('bury_st_edmunds');
          // $gallery->addPopupImage($filename, $title, $text);
          $gallery->addPopupImage('IMG_6954', '', '');
          $gallery->addPopupImage('IMG_6956', '', '');
          $gallery->addPopupImage('IMG_6960', '', '');
          $gallery->addPopupImage('IMG_6963', '', '');
          $gallery->addPopupImage('IMG_6969', '', '');
          $gallery->addPopupImage('IMG_6975', '', '');
          $gallery->addPopupImage('IMG_6976', '', '');
          $gallery->addPopupImage('IMG_6979', '', '');
          ?>
        </ul>
        <p>
          <a href="..">Zur&uuml;ck</a>
        </p>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/body_end_gallery.inc.php'); ?>
</body>
</html>
