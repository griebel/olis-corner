<!DOCTYPE html>
<html>
<head>
  <title>Olis Corner</title>

  <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/head_common.inc.php'); ?>
</head>
<body>
  <div class="frame">
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/header.inc.php'); ?>
    <div class="body">
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/upperbound.inc.php'); ?>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/controlbar_index.inc.php'); ?>
      <!-- ██  ██████  ██████  ███    ██ ████████ ███████ ███    ██ ████████ ██ -->
      <!-- ██ ██      ██    ██ ████   ██    ██    ██      ████   ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██ ██  ██    ██    █████   ██ ██  ██    ██    ██ -->
      <!-- ██ ██      ██    ██ ██  ██ ██    ██    ██      ██  ██ ██    ██    ██ -->
      <!-- ██  ██████  ██████  ██   ████    ██    ███████ ██   ████    ██    ██ -->
      <div class="content">
        <h1>Herzlich Willkommen</h1>
        <p>
          Diese Webseite ist urspr&uuml;nglich aus einem Hochschul-Projekt entstanden.
          Schnell habe ich jedoch gemerkt, dass es sehr n&uuml;tzlich ist eine eigene
          Webseite zu betreiben. Hier kann ich meine eigenen, kleinen Projekte
          vorstellen und eine Bildergalerie &uuml;ber meine Reisen pflegen.
        </p>
        <p>
          Bei Interesse d&uuml;rft ihr selbsverst&auml;ndlich gerne den Links folgen und
          meine Profile durchst&ouml;bern:
        </p>
        <table>
          <tr>
            <td>
              <a href="https://de.linkedin.com/pub/oliver-griebel/120/30a/290"><img src="/include/images/LinkedIn.png" /></a>
            </td>
            <td>
              <a href="https://www.xing.com/profile/Oliver_Griebel"><img src="/include/images/XING.gif" /></a>
            </td>
            <td>
              <a href="https://github.com/olgr"><img src="/include/images/GitHub_Mark.png" height=23 /><img src="/include/images/GitHub_Logo.png" height=23 alt="GitHub" /></a>
            </td>
          </tr>
        </table>
      </div>
      <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/lowerbound.inc.php'); ?>
    </div>
    <?php require_once($_SERVER['DOCUMENT_ROOT'].'/include/footer.inc.php'); ?>
  </div>
</body>
</html>
