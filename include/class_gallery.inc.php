<?php
class Gallery {

  private $galleryname = 'gallery';

  public function setName($galleryname) {
    $this->galleryname = $galleryname;
  }

  public function addPopupImage($filename, $title, $text) {
    echo '<li>';
    echo '<a href="' . $filename . '-small.jpg" data-lightbox="' . $this->galleryname . '" data-title="' . $title . '">';
    echo '<img alt="' . $title . '" src="' . $filename . '-thumb.jpg" />';
    echo '</a>';
    if(strcmp($title, '') !== 0) {
      echo '<h3>' . $title . '</h3>';
    } else {
      echo '<h3>&nbsp;</h3>';
    }
    echo '<p>' . $text . '</p>';
    echo '</li>';
    echo "\r\n";
  }

  public function addClickableImage($folder, $filename, $title, $text) {
    echo '<li>';
    echo '<a href="' . $folder . '">';
    echo '<img alt="' .  $title . '" src="' . $folder . '/' . $filename . '-thumb.jpg" />';
    echo '</a>';
    echo '<h3>' . $title . '</h3>';
    echo '<p>' . $text . '</p>';
    echo '</li>';
    echo "\r\n";
  }
}
?>
