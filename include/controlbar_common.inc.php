<ul id="menu">
  <li><a id="topMenuIndex" href="/index.php">Start</a></li>
  <li class="sub"><a id="topMenuTankmanager" href="#" tabindex="3">Tankmanager</a>
    <div class="hider" id="subTankmanager">
      <ul>
        <li><a id="subMenuTankmanagerHome" href="/tankmanager/">Home</a></li>
        <li><a id="subMenuTankmanagerApp" href="/tankmanager/app.php">App</a></li>
        <li><a id="subMenuTankmanagerRegistration" href="/tankmanager/registration.php">Registration</a></li>
      </ul>
    </div>
  </li>
  <li><a id="topMenuUrbansensingapp" href="/urbansensingapp/">Urban Sensing App</a></li>
  <li class="sub"><a id="topMenuGallery" href="#" tabindex="4">Galerie</a>
    <div class="hider" id="subGallery">
      <ul>
        <li><a id="subMenuGalleryJapan" href="/gallery/japan/">Japan</a></li>
        <li><a id="subMenuGalleryChina" href="/gallery/china/">China</a></li>
        <!-- <li><a id="subMenuGalleryVietnam" href="/gallery/vietnam/">Vietnam</a></li> -->
        <li><a id="subMenuGalleryEngland" href="/gallery/england/">England</a></li>
      </ul>
    </div>
  </li>
  <li><a id="topMenuImpressum" href="/impressum.php">Impressum</a></li>
</ul>
