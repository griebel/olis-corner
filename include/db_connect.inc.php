<?php

/**
*  #####################################################
* 	Autor:                  Oliver Griebel
*  Datum der Erstellung:   01.05.2014
*  Letzte Revision:        18.05.2014
*
*  db_connect.inc.php
*
*  Die Klasse DB_CONNECT ist fuer den Verbindungs-
*	aufbau und -abbau zustaendig.
*  #####################################################
*/

class DB_CONNECT {

	/*
	*Konstruktor
	*/
	function __construct() {
		// Verbindungsaufbau zur Datenbank
		$this->connect();
	}

	/*
	* Destruktor
	*/
	function __destruct() {
		// Verbindungsabbau zur Datenbank
		$this->close();
	}

	/*
	* Funktion zum Verbindungsaufbau mit der Datendank
	*/
	function connect() {

		// importieren der Zugangsdaten
		require_once 'db_config.inc.php';

		// Verbinden mit mysql Datenbank
		$con = mysql_connect(DB_SERVER, DB_USER, DB_PASSWORD) or die(mysql_error());

		// Auswahl der Datenbank
		$db = mysql_select_db(DB_DATABASE) or die(mysql_error());

		// Rueckgabe des Verbindungsaufbaus
		return $con;
	}

	/*
	* Funktion zum Verbindungsabbau mit der Datenbank
	*/
	function close() {
		// Schlssen der Datenbank-Verbindung
		mysql_close();
	}

}

?>
